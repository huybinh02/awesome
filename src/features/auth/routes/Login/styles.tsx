import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  wraper: {
    flex: 1
  },
  mt20: {
    marginTop: 20
  },
  logo: {
    alignSelf: 'center'
  },

  content_header: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  content: {
    alignItems: 'center',
    alignSelf: 'center',
    flexDirection: 'row',
    marginTop: 5
  },
  markPincode: {
    width: 10,
    height: 10,
    borderRadius: 25,
    backgroundColor: 'white'
  }
})

export default styles
