import React from 'react'
import { View } from 'react-native'
import InputView from '@src/components/InputView'
import styles from '@src/features/auth/routes/Login/styles'
const LoginScreen = () => {
  return (
    <View style={styles.wraper}>
      <InputView />
    </View>
  )
}

export default LoginScreen
