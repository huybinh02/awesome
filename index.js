/**
 * @format
 */

import { AppRegistry, TextInput, Text } from 'react-native'
import App from './App'
import { name as appName } from './app.json'
import { LoginScreen } from '@src/features/auth'
const RootApp = () => {
  return <LoginScreen />
}
AppRegistry.registerComponent(appName, () => RootApp)
console.disableYellowBox = true
TextInput.defaultProps = TextInput.defaultProps || {}
TextInput.defaultProps.allowFontScaling = false
Text.defaultProps = Text.defaultProps || {}
Text.defaultProps.allowFontScaling = false
